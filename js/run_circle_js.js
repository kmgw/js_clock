/* run_circle_js */
{
    // 画面に表示する泡の数
    const circleNum = 100;
    // 画面に表示するDOM格納配列
    let circle = Array(circleNum).fill(0);
    // 初期描画数カウント
    let count = 0;

    // 本体要素定義
    const jsCircle = $("<div>").addClass("run_background_container");


    // 泡のプロパティ生成
    const circleProperty =()=> {
        
        // 泡の大きさ（4~16）
        let size        = Math.floor(Math.random() * (8 - 2) + 2) * 2;
        // 泡の不透明度（0.1~0.9）
        let opacity     = (Math.random() * (0.9 - 0.1) + 0.1).toFixed(1);
        // 泡の生成位置横方向
        let positionX   = Math.floor(Math.random() * $(window).outerWidth(true));
        // 泡が上に上る所要時間(6~12秒)
        let move        = Math.floor(Math.random() * (12 - 6) + 6);
        // 泡の横揺れと遅れ所要時間(1~2秒)
        let fluctuation = parseFloat((Math.random() * (2 - 1) + 1).toFixed(1));

        let circleCss   = {
            'width'        : size + 'px',
            'height'       : size + 'px',
            'border-radius': size/2 + 1 + 'px',
            'opacity'      : opacity,
            // 縦方向のデフォルト表示位置を指定しないと、アニメーションが始まるまで上に表示されるので必ず指定
            'top'          : 'calc(100% + ' + size + 2 + ')',
            'left'         : positionX + 'px',
            'animation'    : 'circle-move '+ move + 's linear ' + fluctuation + 's infinite normal, circle-fluctuation ' + fluctuation + 's ease-in-out 1s infinite normal'
        }

        return {'time' : move + fluctuation, 'circleCss' : circleCss}; 
    }


    // 泡生成
    const createCircle = (index) => {

        // 泡に設定するCSSとアニメーション時間を取得
        let property = circleProperty(); 
        let time = property.time * 1000;

        circle[index] = $("<div>").addClass("circle").css(property.circleCss);
        $(jsCircle).append(circle[index]);

        // 泡が上まで上ったらDOMを削除してこのメソッドをループ
        setTimeout(() =>{
            circle[index].remove();
            createCircle(index);
        }, time);
    }


    // 最初にばらけて表示させるため、createCircleの実行タイミングをずらす
    const firstCreateCircle =()=> {
        if (count < circleNum) {
            createCircle(count);
            count++;
            setTimeout(firstCreateCircle, 200);
        }
    }


    // 起動処理
    {
        $("body").append(jsCircle);
        firstCreateCircle();    
    }
}
