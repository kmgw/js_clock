/* run_clock_js */
{
    // 時間obj class Clockのインスタンス変数
    const clockIndex = ['hour', 'minute', 'seconds'];

    class Clock {

        constructor (hour = null, minute = null, seconds = null) {

            this.clock = {};
            
            for (let i in clockIndex) {

                if (arguments[i] != undefined) {
                    this.clock[clockIndex[i]] = arguments[i];
                } else {
                    this.clock[clockIndex[i]] = null;
                }
            }
        }
        
        get prop(){
            return this.clock;
        }

        setValue(index, value) {
            this.clock[index] = value;
        }
    }

    // アナログ時計の本体要素定義
    const jsClockAnalog = $('<div>').addClass('run_clock_circle');

    // デジタル時計の本体要素定義
    const jsDigitalContent = $('<div>').addClass('run_clock_digital');
    const jsCalendar = $('<div>').addClass('clock_calendar');
    const jsDigitalClock = $('<div>').addClass('clock_dg_clock');

    // 色時計の本体要素定義
    const jsClockColor = $('<div>').addClass('run_clock_color');

    // アナログ時計、デジタル時計、色時計を画面にセット
    $('body').append(jsClockAnalog, jsDigitalContent, jsClockColor);
    // デジタル時計の子要素（カレンダー、デジタル時計）をセット
    $(jsDigitalContent).append(jsCalendar, jsDigitalClock);

    // 時間、分、秒の固定親DOM
    let jsClockBaseBody = new Clock();
    // 時間、分、秒の回転アニメーションDOM
    let jsClockBody = new Clock();
    // 時間、分、秒の針
    let jsClockPoint = new Clock();
    // 曜日定義
    const weekStr = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    // 時間objをそれぞれ生成
    for (let i in clockIndex) {
        jsClockBaseBody.setValue(clockIndex[i], $('<div>').addClass('run_clock_base'));
        jsClockBody.setValue(clockIndex[i], $('<div>').addClass('run_clock_body'));
        jsClockPoint.setValue(clockIndex[i], $('<div>').addClass('run_clock_' + clockIndex[i]));
        // アナログ時計の子要素をセット
        $(jsClockAnalog).append(jsClockBaseBody.prop[clockIndex[i]]);
        $(jsClockBaseBody.prop[clockIndex[i]]).append($(jsClockBody.prop[clockIndex[i]]).append(jsClockPoint.prop[clockIndex[i]]));
    }


    // 現在時刻のClock objを返す
    const getTime =()=> {

        let now = new Date();
        let time = new Clock(now.getHours(), now.getMinutes(), now.getSeconds());
        time.setValue('year', now.getFullYear());
        time.setValue('month', now.getMonth() + 1);
        time.setValue('day', now.getDate());
        time.setValue('weekDay', weekStr[now.getDay()] + '.');
        return time.prop;
    }


    // 色時計のCSS設定
    const setCssClock =(time)=> {

        let colorCheckNum = parseFloat(time.hour) + (time.minute / 60);

        let color = {
            'r' : 0,
            'g' : 0,
            'b' : 0,
        }

        if (time.hour <= 4) {

            color.r = 255 - (colorCheckNum * 63.8);
            color.b = 255;

        } else if (time.hour <= 8) {

            color.g = Math.abs(4 - colorCheckNum) * 63.8;
            color.b = 255;

        } else if (time.hour <= 12) {

            color.g = 255;
            color.b = 255 - (Math.abs(8 - colorCheckNum) * 63.8);

        } else if (time.hour <= 16) {

            color.g = 255;
            color.r = Math.abs(12 - colorCheckNum) * 63.8;

        } else if (time.hour <= 20) {

            color.r = 255;
            color.g = 255 - (Math.abs(16 - colorCheckNum) * 63.8);

        } else if (time.hour <= 24) {

            color.r = 255;
            color.b = Math.abs(20 - colorCheckNum) * 63.8;

        }

        for (let i in color) {
            if (color[i] >= 255) {
                color[i] = 255;
            }
            color[i] = Math.abs(parseInt(color[i]));
        }

        $(jsClockColor).css({'background' : 'linear-gradient(#fff, rgb(' + color.r + ',' + color.g + ',' + color.b + '))'});
    }


    // 毎秒起動するメソッド
    const runSeconds =()=> {

        let time = getTime();

        // 色時計のCSS再設定
        setCssClock(time);

        // 年月日、曜日、時間、分、秒
        for (let i in time) {
            // 1桁の数値は2桁になるよう0埋めする
            if (typeof time[i] == 'number' && time[i] < 10) {
                time[i] = '0' + String(time[i]);
            }
        }

        $(jsCalendar).text(time.year + '/' + time.month + '/' + time.day + ' ' + time.weekDay);
        $(jsDigitalClock).text(time.hour + ':' + time.minute);

        // 再起
        setTimeout(runSeconds, 1000);
    }


    // アナログ時計の傾きをセットする
    const setFirstAngle =()=> {

        let time = getTime();

        // 秒も含めた現在時刻の分
        let minute = parseFloat(time.minute) + parseFloat(time.seconds / 60);
        // 秒、分も含めた現在時刻の時間
        let hour = parseFloat(time.hour) + minute / 60;

        // jsClockBaseBodyで現在時刻に合わせて傾ける
        // 子要素のjsClockBodyは常に0～360度を一定間隔で動く
        $(jsClockBaseBody.prop.hour).css({'transform' : 'rotate(' + parseInt(hour * 30) + 'deg)'});
        $(jsClockBaseBody.prop.minute).css({'transform' : 'rotate(' + parseInt(minute * 6) + 'deg)'});
        $(jsClockBaseBody.prop.seconds).css({'transform' : 'rotate(' + parseInt(time.seconds * 6) + 'deg)'});
    }


    // 起動処理
    {
        // アナログ時計の傾きをセット
        setFirstAngle();
        // 毎秒ごとの処理起動
        runSeconds();
    }
}
